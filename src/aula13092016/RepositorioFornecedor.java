package aula13092016;

public class RepositorioFornecedor {

	private Fornecedor[] listaFornecedor;

	public RepositorioFornecedor() {
		listaFornecedor = new Fornecedor[1000];
	}

	public boolean inserirFornecedor(Fornecedor fornecedor) {

		for (int i = 0; i < listaFornecedor.length; i++) {
			if (listaFornecedor[i] != null && listaFornecedor[i].getCnpj().equals(fornecedor.getCnpj())) {

				return false;
			}
		}

		for (int i = 0; i < listaFornecedor.length; i++) {
			if (listaFornecedor[i] == null) {
				listaFornecedor[i] = fornecedor;
				return true;
			}
		}

		return false;

	}

	public boolean removerFornecedorPorCnpj(String cnpj) {
		for (int i = 0; i < listaFornecedor.length; i++) {
			if (listaFornecedor[i] != null && listaFornecedor[i].getCnpj().equals(cnpj)) {
				listaFornecedor[i] = null;
				return true;
			}
		}
		return false;
	}

	public Fornecedor[] pesquisarTipodeservico(int TipoServico) {
		Fornecedor[] fornecedores = null;
		int count = 0;
		for (int i = 0; i < listaFornecedor.length; i++) {
			if (listaFornecedor[i].getTipoDeServico() == TipoServico) {
				count++;
			}
		}
		if (count > 0) {
			int pos = 0;
			fornecedores = new Fornecedor[count];
			for (int i = 0; i < listaFornecedor.length; i++) {
				if (listaFornecedor[i] != null && listaFornecedor[i].getTipoDeServico() == TipoServico) {

					fornecedores[pos] = listaFornecedor[i];
					pos++;

				}
			}
		}
		return fornecedores;
	}

	public Fornecedor[] pesquisarCep(String cep) {
		Fornecedor[] fornecedores = null;
		int count = 0;

		for (int i = 0; i < listaFornecedor.length; i++) {
			if (listaFornecedor[i] != null && listaFornecedor[i].getEndereco().getCep().equals(cep)) {
				count++;
			}
		}
		if (count > 0) {
			int pos = 0;
			fornecedores = new Fornecedor[count];
			for (int i = 0; i < listaFornecedor.length; i++) {
				if (listaFornecedor[i] != null && listaFornecedor[i].getEndereco().getCep().equals(cep)) {

					fornecedores[pos] = listaFornecedor[i];
					pos++;

				}
			}
		}
		return fornecedores;
	}

}
