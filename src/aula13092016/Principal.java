package aula13092016;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Principal {

	static Scanner scanner;
	static RepositorioCliente repositorioCliente;
	static RepositorioFornecedor repositorioFornecedor;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		// Garanta que essa � a �nica instancia
		repositorioCliente = new RepositorioCliente();
		repositorioFornecedor = new RepositorioFornecedor();
		double menu = 0;
		double funcionalidadesCliente = 0;
		double funcionalidadesFornecedor = 0;

		while (menu < 3) {

			menu = Integer.parseInt(JOptionPane
					.showInputDialog("1 Cadastro de Cliente \n 2 Cadastro de Fornecedor \n 3 Sair do Sistema "));

			if (menu == 1) {

				while (funcionalidadesCliente < 6) {

					funcionalidadesCliente = Integer.parseInt(JOptionPane.showInputDialog(
							"1 Inserir Cliente \n 2 Remover Cliente via c�digo \n 3 Remover Cliente via nome"
									+ "\n 4 Pesquisar Cliente por c�digo" + "\n 5 Pesquisar clientes por bairro"));

					if (funcionalidadesCliente == 1) {
						inserirCliente();
						JOptionPane.showMessageDialog(null, "Cliente Cadastrado");

					} else if (funcionalidadesCliente == 2) { 
						funcionalidadesCliente = Integer
								.parseInt(JOptionPane.showInputDialog("Digite o codigo para remover:"));
						repositorioCliente.removerClienteporCodigo(0);
						JOptionPane.showMessageDialog(null, "Cliente Removido via c�digo");

					} else if (funcionalidadesCliente == 3) {
						funcionalidadesCliente = Integer
								.parseInt(JOptionPane.showInputDialog("Digite o Nome para remover:"));
						repositorioCliente.RemoverClienteporNome(null);
						JOptionPane.showMessageDialog(null, "Cliente Removido via nome");

					} else if (funcionalidadesCliente == 4) {
						funcionalidadesCliente = Integer
								.parseInt(JOptionPane.showInputDialog("Digite o codigo para pesquisar:"));
						repositorioCliente.pesquisarCodigo(0);
						JOptionPane.showMessageDialog(null,
								"Cliente pesquisado via C�digo: " + repositorioCliente.pesquisarCodigo(0));

					} else if (funcionalidadesCliente == 5) {
						funcionalidadesCliente = Integer
								.parseInt(JOptionPane.showInputDialog("Digite o Bairro para pesquisar:"));
						repositorioCliente.pesquisarBairro(null);
						JOptionPane.showMessageDialog(null,
								"Cliente pesquisado via Bairro: " + repositorioCliente.pesquisarBairro(null));

					}

				}
			} else if (menu == 2) {

				while (funcionalidadesFornecedor < 5) {

					funcionalidadesFornecedor = Integer.parseInt(JOptionPane.showInputDialog(
							"1 Inserir Fornecedor \n 2 Remover Fornecedor \n 3 Pesquisar Fornecedor por Servi�o"
									+ " \n 4 Pesquisar Fornecedor por CEP"));

					if (funcionalidadesFornecedor == 1) {
						inserirFornecedor();
						JOptionPane.showMessageDialog(null, "Fornecedor Cadastrado");

					} else if (funcionalidadesFornecedor == 2) {
						funcionalidadesFornecedor = Integer
								.parseInt(JOptionPane.showInputDialog("Digite o Cnpj para remover:"));
						repositorioFornecedor.removerFornecedorPorCnpj(null);
						JOptionPane.showMessageDialog(null, "Fornecedor Removido via Cnpj");

					} else if (funcionalidadesFornecedor == 3) {
						funcionalidadesFornecedor = Integer
								.parseInt(JOptionPane.showInputDialog("Digite o TIpo De Servico para pesquisar:"));
						repositorioFornecedor.pesquisarTipodeservico(0);
						JOptionPane.showMessageDialog(null, "Fornecedor Pesquisado Via Tipo De Servico: " +repositorioFornecedor
								.pesquisarTipodeservico(0));

					} else if (funcionalidadesFornecedor == 4) {
						funcionalidadesFornecedor = Integer
								.parseInt(JOptionPane.showInputDialog("Digite o CEP para pesquisar:"));
						repositorioFornecedor.pesquisarCep(null);
						JOptionPane.showMessageDialog(null, "Fornecedor Pesquisado Via Cep " +repositorioFornecedor
								.pesquisarCep(null));

					}
				}
			}
		}
		JOptionPane.showMessageDialog(null, "Voc� acabou de sair do sistema");

	}

	public static void inserirCliente() {
		Cliente c = new Cliente();
		System.out.println("Digite o c�digo");
		c.setCodigo(scanner.nextInt());
		System.out.println("Digite o nome");
		c.setNome(scanner.next());
		System.out.println("Digite o email");
		c.setEmail(scanner.next());
		System.out.println("Digite o cpf");
		c.setCpf(scanner.next());
		System.out.println("Digite a data de nascimento");
		c.setDataNascimento(scanner.next());

		// Cria o objeto endere�o e cadastra os valores
		Endereco end = new Endereco();
		System.out.println("Endere�o - Digite a rua");
		end.setRua(scanner.next());
		System.out.println("Endere�o - Digite o bairro");
		end.setBairro(scanner.next());
		System.out.println("Endere�o - Digite o numero");
		end.setNumero(scanner.nextInt());
		System.out.println("Endere�o - Digite o complemento");
		end.setComplemento(scanner.next());
		System.out.println("Endere�o - Digite o cep");
		end.setCep(scanner.next());

		// Seta o endere�o criado no objeto cliente
		c.setEndereco(end);

		repositorioCliente.inserirCliente(c);
	}

	public static void inserirFornecedor() {
		Fornecedor c = new Fornecedor();
		System.out.println("Digite o c�digo");
		c.setTipoDeServico(scanner.nextInt());
		System.out.println("Digite o nome");
		c.setNome(scanner.next());
		System.out.println("Digite o nomeFantasia");
		c.setNomeFantasia(scanner.next());
		System.out.println("Digite o email");
		c.setEmail(scanner.next());
		System.out.println("Digite o cnpj");
		c.setCnpj(scanner.next());

		// Cria o objeto endere�o e cadastra os valores
		Endereco end = new Endereco();
		System.out.println("Endere�o - Digite a rua");
		end.setRua(scanner.next());
		System.out.println("Endere�o - Digite o bairro");
		end.setBairro(scanner.next());
		System.out.println("Endere�o - Digite o numero");
		end.setNumero(scanner.nextInt());
		System.out.println("Endere�o - Digite o complemento");
		end.setComplemento(scanner.next());
		System.out.println("Endere�o - Digite o cep");
		end.setCep(scanner.next());

		// Seta o endere�o criado no objeto Fornecedor
		c.setEndereco(end);

		repositorioFornecedor.inserirFornecedor(c);

	}

}
