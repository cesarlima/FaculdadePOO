package aula13092016;

public class RepositorioCliente {

	private Cliente[] listaClientes;

	public RepositorioCliente() {
		listaClientes = new Cliente[100];
	}

	public boolean inserirCliente(Cliente c) {

		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getCpf().equals(c.getCpf())
					|| listaClientes[i].getCodigo() == (c.getCodigo())) {

				return false;
			}
		}

		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] == null) {
				listaClientes[i] = c;
				return true;
			}
		}

		return false;

	}

	public boolean removerClienteporCodigo(int codigo) {
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getCodigo() == codigo) {
				listaClientes[i] = null;
				return true;
			}
		}return false;
	} 

	public int RemoverClienteporNome(String Nome) {
		int count = 0;
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getNome().equals(Nome)) {
				listaClientes[i] = null;
				// return true;
				count++;
			}
		}
		return count;
	}

	public Cliente pesquisarCodigo(int codigo) {
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getCodigo() == codigo) {
				listaClientes[i] = null;
				return listaClientes[i];
			}
		}
		return null;
	}

	public Cliente pesquisarBairro(String bairro) {
		Cliente[] clientes = null;
		int cont = 0;
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getEndereco().getBairro().equals(bairro)) {
				cont++;
			}
		}
		if (cont > 0) {
			clientes = new Cliente[cont];
			int position = 0;
			for (int i = 0; i < listaClientes.length; i++) {
				if (listaClientes[i] != null && listaClientes[i].getEndereco().getBairro().equals(bairro)) {
					clientes[position] = listaClientes[i];
					position++;
				}

			}
		}
		return null;
	}

}
