
package aula;

import javax.swing.JOptionPane;

public class ETUDOPUTA {

	public static void main(String[] args) {
		double nota1;
		double nota2;

		nota1 = Double.parseDouble(JOptionPane.showInputDialog("Digite a nota 1"));
		nota2 = Double.parseDouble(JOptionPane.showInputDialog("Digite a nota 2"));

		double media = (nota1 + nota2) / 2;

		if (media >= 7 && media <= 10) {
			JOptionPane.showMessageDialog(null, "Aprovado");
		} else if (media >= 0 && media < 4 ) {
			JOptionPane.showMessageDialog(null, "Reprovado");
		} else {
			double notafinal = Double.parseDouble(JOptionPane.showInputDialog("Digite a nota da final"));

			double mediaAposFinal = (media + notafinal) / 2;

			if (mediaAposFinal > 5) {
				JOptionPane.showMessageDialog(null,"Aprovado Ap�s final");
			} else {
				JOptionPane.showMessageDialog(null,"Reprovado Ap�s Final");
			}
		}
	}
}