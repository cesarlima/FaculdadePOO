package aula;

import javax.swing.JOptionPane;

public class Media {

	public static void main(String[] args) {
		float nota1; // VARIAVEL
		float nota2; // VARIAVEL
		float media; // VARIAVEL
		nota1 = Float.parseFloat(JOptionPane.showInputDialog("digite a Nota 1")); // NOTA1
		nota2 = Float.parseFloat(JOptionPane.showInputDialog("digite a Nota 2")); // NOTA2
		media = (nota1 + nota2) / 2;

		if (media >= 7) {
			JOptionPane.showMessageDialog(null, " Aprovado por media " + media); // DIALOGO
		} else if (media >= 4 && media < 7) {
			JOptionPane.showMessageDialog(null, " Prova Final " + media); // DIALOGO
		} else {
			JOptionPane.showMessageDialog(null, " Reprovado por media " + "" + media); // DIALOGO
		}
	}
}
