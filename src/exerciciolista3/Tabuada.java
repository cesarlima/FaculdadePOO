package exerciciolista3;

import javax.swing.JOptionPane;

public class Tabuada {

	public static void main(String[] args) {

		int num = 0, cont = 0, mult = 0;

		num = 3;

		while (num <= 6) {
			JOptionPane.showMessageDialog(null, "tabuada de: " + num);
			cont = 1;
			
			while (cont <= 10) {
				JOptionPane.showMessageDialog(null, num + "X" + cont + "=" + mult);
				mult = num * cont;
				cont = cont + 1;
			}
			num = num + 1;
		}
	}
}
