package exerciciolista3;

import javax.swing.JOptionPane;

public class Questao3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numero;
		numero=Integer.parseInt(JOptionPane.showInputDialog("Digite um numero qualquer : "));
		
		do {
			JOptionPane.showMessageDialog(null, "Numero Incorreto");
			numero=Integer.parseInt(JOptionPane.showInputDialog("Digite um numero qualquer : "));
			
		}while((numero<1)||(numero>100));
		
		JOptionPane.showMessageDialog(null, "Numero esta entre 1 e 100.");
		
	}

}
