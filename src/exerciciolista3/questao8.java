package exerciciolista3;

import java.util.Scanner;

public class questao8 {

	public static void main(String[] args) {
		Scanner leTeclado = new Scanner(System.in);
		double valor;
		int escolha;
		boolean laco = true;

		System.out.println("Bem vindo!");

		while (laco) {

			System.out.println("Digite o valor a ser investido.");
			valor = leTeclado.nextDouble();
			leTeclado.nextLine();

			if (valor <= 0) {
				laco = false;
			} else {

				System.out.println("Valor digitado foi: " + valor);
				System.out.println("Qual tipo de investimento deseja fazer?");

				System.out.println("1- Poupan�a: Valor M�nimo: R$100,00, renda 0.5%/m�s");
				System.out.println("2- Fundo de Renda Fixa: Valor M�nimo: R$ 1.000,00, renda 1%/m�s");
				System.out.println("3- CDB, Valor M�nimo: R$2.500,00, renda 2,5%/M�s");
				System.out.println("OBS: pode ser escolhido mais de 1 op��o.");
				escolha = leTeclado.nextInt();

				switch (escolha) {
				case 1:
					System.out.println("De acordo com seu investimento voc� ter�: " + (valor * 0.005)
							+ " de lucro para investimento tipo poupan�a.");
					break;

				case 2:
					System.out.println("De acordo com seu investimento voc� ter�: " + (valor * 0.01)
							+ " de lucro para investimento tipo Fundo de Renda Fixa.");
					break;

				case 3:
					System.out.println("De acordo com seu investimento voc� ter�: " + (valor * 0.025)
							+ " de lucro para investimento tipo CDB.");
					break;

				default:
					System.err.println("N�o existe esse tipo de investimento, favor repita a opera��o!");
					break;
				}
			}
		}

		System.out.println("Programa finalizado!");
	}
}
