package Listadexercicio30082016;

import javax.swing.JOptionPane;

public class Questao3 {

	public static void main(String[] args) {

		int contPar = 0, contImpar = 0;
		int[][] matriz = new int[3][3];

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				matriz[i][j] = Integer.parseInt(JOptionPane.showInputDialog(null, "valor:"));
			}
		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				JOptionPane.showMessageDialog(null, "Matriz: " + matriz[i][j]);

				if (matriz[i][j] % 2 == 0) {

					contPar++;

					JOptionPane.showMessageDialog(null, "Foram digitados: " + contPar + "numeros pares");

				}

				if (matriz[i][j] % 2 == 1) {

					contImpar++;

					JOptionPane.showMessageDialog(null, "Foram digitados: " + contImpar + "numeros impares");
				}
			}
		}

	}

}
