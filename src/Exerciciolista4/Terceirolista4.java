package Exerciciolista4;

import javax.swing.JOptionPane;

public class Terceirolista4 {

	// 3� Programa para calcular o fatorial .
	public static void main(String[] args) {
		int fat = 1;
		int valor = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor do fatoria "));
		for (int i = valor; i > 1; i--) {
			fat *= i;
		}
		JOptionPane.showMessageDialog(null, "O fatorial de " + valor + " � igual a " + fat);
	}
}
