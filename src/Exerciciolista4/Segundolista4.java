package Exerciciolista4;

import javax.swing.JOptionPane;

public class Segundolista4 {

	public static void main(String[] args) {
		// programa para calcular �rea e volume de determinadas figuras
		// geom�tricas FICHA 4 SEGUNDA QUEST�O .
		int opera�ao;
		opera�ao = Integer.parseInt(JOptionPane
				.showInputDialog("Digite 1 para calcular �rea da figura e 2 para saber o volume da figura "));
		if (opera�ao == 1) {
			int meio;
			meio = Integer.parseInt(JOptionPane.showInputDialog(
					"Digite 1 para area do circulo , 2 para triangulo , 3 para ret�ngulo e 4 para trap�zio"));
			if (meio == 1) {
				double raio; // vari�veis para ler os determinados valores

				double base;
				raio = Double.parseDouble(JOptionPane.showInputDialog("digite o raio da circunferencia "));
				base = (2 * Math.PI * raio * raio);
				JOptionPane.showMessageDialog(null, "area � " + base);
			} else if (meio == 2) {
				double haltura = Double.parseDouble(JOptionPane.showInputDialog("digite a altura do triangulo"));
				double bases = Double.parseDouble(JOptionPane.showInputDialog("digite a base do triangulo"));
				double resultado = (bases * haltura) / 2;
				JOptionPane.showMessageDialog(null, "area � " + resultado);
			} else if (meio == 3) {
				double autura = Double.parseDouble(JOptionPane.showInputDialog("digite a altura do triangulo"));
				double b = Double.parseDouble(JOptionPane.showInputDialog("digite a base do triangulo"));
				double result = (autura * b);
				JOptionPane.showMessageDialog(null, "�rea � " + result);
			} else if (meio == 4) {
				double basemaior = Double.parseDouble(JOptionPane.showInputDialog("digite a base maior do trap�zio"));
				double basemenor = Double.parseDouble(JOptionPane.showInputDialog("digite a base menor do trap�zio"));
				double alturatrapezio = Double.parseDouble(JOptionPane.showInputDialog("digite a altura do trap�zio"));
				double conclus�o = (basemaior + basemenor) * alturatrapezio / 2;
				JOptionPane.showMessageDialog(null, "�rea � " + conclus�o);
			}
		}
		if (opera�ao == 2) {
			int vo;
			vo = Integer.parseInt(JOptionPane.showInputDialog(
					"Digite 1 para volume do esfera , 2 para cilindro , 3 para cubo e 4 para paralelep�pedo"));
			if (vo == 1) {
				double raioesfera = Double.parseDouble(JOptionPane.showInputDialog("digite o raio da esfera"));
				double volumeesfera = (4 * Math.PI * raioesfera * raioesfera * raioesfera) / 3;
				JOptionPane.showMessageDialog(null, "o volume da esfera � " + volumeesfera);
			} else if (vo == 2) {
				double alturacilindro = Double.parseDouble(JOptionPane.showInputDialog("digite a altura do cilindro"));
				double basecilindro;
				double raiocilindro = Double
						.parseDouble(JOptionPane.showInputDialog("digite o raio da circunferencia "));
				basecilindro = (2 * Math.PI * raiocilindro * raiocilindro);
				double resultadocilindro = basecilindro * alturacilindro;
				JOptionPane.showMessageDialog(null, "o volume do cilindro � " + resultadocilindro);

			} else if (vo == 3) {
				double aresta = Double.parseDouble(JOptionPane.showInputDialog("digite ao valor da aresta do cubo "));
				double volumecubo = aresta * aresta * aresta;
				JOptionPane.showMessageDialog(null, "o volume do cubo � " + volumecubo);
			} else if (vo == 4) {
				double arestaA = Double
						.parseDouble(JOptionPane.showInputDialog("digite ao valor da aresta A do paralelograma "));
				double arestaB = Double
						.parseDouble(JOptionPane.showInputDialog("digite ao valor da aresta B do paralelograma "));
				double arestaC = Double
						.parseDouble(JOptionPane.showInputDialog("digite ao valor da aresta C do paralelograma "));
				double volparalelograma = arestaA * arestaB * arestaC;
				JOptionPane.showMessageDialog(null, "o volume do paralelepipedo  � " + volparalelograma);
			}
		}

	}
}
